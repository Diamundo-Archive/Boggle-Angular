import { Injectable } from '@angular/core';
import { Boggle } from './boggle';
import { BoggleLetter } from './boggle-letter';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'

import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class BoggleService {

	private ApiURL = 'http://www.diamundo.nl:3000';
	// /highscore (POST/GET), /checkword?word=X (GET), /newboard (GET)
	const httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	getLetters() : Observable<String> {
		return this.http.get(this.ApiURL + '/newboard');
	}
	
	checkValidWord( word: string ) : Observable<String> {
		return this.http.get(this.ApiURL + '/checkword?word=' + word.toLowerCase());
	}
	
	addHighscore( name: string, points: number, date: string ): Observable<String> {
		return this.http.post(this.ApiURL, { Name: name, Score: points, DateTime: date }, this.httpOptions);
	}
	
	constructor(
		private http: HttpClient
	) { }

}
